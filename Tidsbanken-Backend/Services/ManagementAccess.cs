﻿using Auth0.ManagementApi;
using Auth0.ManagementApi.Models;
using Newtonsoft.Json.Linq;
using PasswordGenerator;
using RestSharp;
using Tidsbanken_Backend.Models.DTOs.Admin;

namespace Tidsbanken_Backend.Services
{
    public static class ManagementAccess
    {
        public static IRestResponse GetManagementAccessToken()
        {
            var client = new RestClient("https://dev-cfseaabn.eu.auth0.com/oauth/token");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", "{\"client_id\":\"KdsQcW5Vb575sLEfIaX3mR01HKDTEqbg\",\"client_secret\":\"1jOxrUu-F4qYyPWGleRsEoCu_6CcMGyVxV7EtxkAA1qVMkwcmcXhxey5myv1kGC8\",\"audience\":\"https://dev-cfseaabn.eu.auth0.com/api/v2/\",\"grant_type\":\"client_credentials\"}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response;
        }

        public static async Task<string> CreateApiManagement(AdminCreateUserDTO dto)
        {
            IRestResponse token = GetManagementAccessToken();
            var accesstoken = JObject.Parse(token.Content).GetValue("access_token");
            var client = new ManagementApiClient((string)accesstoken, "dev-cfseaabn.eu.auth0.com");
            var password = new Password(includeLowercase: true, includeUppercase: true, includeNumeric: true, includeSpecial: true, passwordLength: 16).Next();
            var user = new UserCreateRequest()
            {
                Email = dto.Email,
                Password = password,
                Connection = "tidsbanken-db",
                FullName = dto.FirstName + " " + dto.LastName,
            };
            var response = await client.Users.CreateAsync(user);
            return response.UserId;
        }
    }
}