﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Tidsbanken_Backend.Models.Data;
using Tidsbanken_Backend.Models.Domain;
using Tidsbanken_Backend.Models.DTOs.Self;
using Tidsbanken_Backend.Models.DTOs.Admin;
using Tidsbanken_Backend.Services;
using Tidsbanken_Backend.Models.DTOs;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace Tidsbanken_Backend.Controllers;
[ApiController]
[Route("Admin/Request")]
public class AdminRequestController : ControllerBase
{

    public AdminRequestController(IMapper mapper, TidsbankenDbContext context)
    {
        _mapper = mapper;
        _context = context;
        _service = new GenericDbService<VacationRequest, int>(context, mapper, (x) => x.VacationRequests);
    }
    private IMapper _mapper;
    private TidsbankenDbContext _context;
    private GenericDbService<VacationRequest, int> _service;

    [Authorize, HttpGet("ReadById/{id}")]
    public async Task<ActionResult<AdminReadVacationRequestDTO>> ReadById(int id)
    {
        if (!(await this.Extension_IsAdmin(_context)))
        {
            return Forbid();
        }
        return await _service.ReadById<AdminReadVacationRequestDTO>(id);
    }
    [Authorize, HttpGet("ReadAll/")]
    public async Task<ActionResult<IEnumerable<AdminReadVacationRequestDTO>>> ReadAll()
    {
        if (!(await this.Extension_IsAdmin(_context)))
        {
            return Forbid();
        }
        var output = from request in _service.ReadAll() select _mapper.Map<AdminReadVacationRequestDTO>(request);
        return Ok(output);
    }
    [Authorize, HttpDelete("Delete/{id}")]
    public async Task<IActionResult> Delete(int id)
    {
        if (!(await this.Extension_IsAdmin(_context)))
        {
            return Forbid();
        }
        // Delete the request's status
        var status = await (from i in _context.VacationStatuses
                            where i.Id == id
                            select i).FirstOrDefaultAsync();
        if (status == null)
        {
            return NotFound();
        }
        _context.VacationStatuses.Remove(status);

        // delete the request
        return await _service.Delete(id);
    }
    [Authorize, HttpGet("GetStatus/{RequestId}")]
    public async Task<ActionResult<ReadVacationStatusDTO>> GetVacationStatus(int RequestId)
    {
        if (!(await this.Extension_IsAdmin(_context)))
        {
            return Forbid();
        }
        var status = (await _context.VacationRequests
            .Include(i => i.VacationStatus)
            .Where(i => i.Id == RequestId)
            .FirstAsync()).VacationStatus;

        return _mapper.Map<ReadVacationStatusDTO>(status);
    }
    [Authorize, HttpPut("SetStatus")]
    public async Task<IActionResult> SetVacationStatus(AdminUpdateVacationStatusDTO status)
    {
        if (!(await this.Extension_IsAdmin(_context)))
        {
            return Forbid();
        }
        if (!StatusHelper.IsValidStatus(status.Status))
        {
            return BadRequest("Invalid status value. Must be " + StatusHelper.Pending + ", " + StatusHelper.Approved + " or " + StatusHelper.Denied + ".");
        }

        var VacationRequest = (await _context.VacationRequests
            .Include(i => i.VacationStatus)
            .Where(i => i.Id == status.Id)
            .FirstAsync());

        if (VacationRequest.UserId == this.Extension_GetUserId())
        {
            return Forbid("Admin can't approve their own requests.");
        }
        var VacationStatus = VacationRequest.VacationStatus;
        if (VacationStatus == null)
        {
            return NotFound();
        }
        _context.Update(VacationStatus);
        _mapper.Map(status, VacationStatus);

        await _context.SaveChangesAsync();

        return Ok();
    }
}
