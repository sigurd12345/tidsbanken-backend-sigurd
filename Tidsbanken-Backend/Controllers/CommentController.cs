﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Tidsbanken_Backend.Models.Data;
using Tidsbanken_Backend.Models.Domain;
using Tidsbanken_Backend.Services;
using Tidsbanken_Backend.Models.DTOs.IneligiblePeriodDTO;
using Tidsbanken_Backend.Models.DTOs.CommentDTO;
using Microsoft.AspNetCore.Authorization;

namespace Tidsbanken_Backend.Controllers;

[ApiController]
[Route("Comments")]
public class CommentController : ControllerBase
{
    public CommentController(IMapper mapper, TidsbankenDbContext context)
    {
        _mapper = mapper;
        _context = context;
        _service = new GenericDbService<Comment, int>(context, mapper, (x) => x.Comments);
    }
    IMapper _mapper;
    TidsbankenDbContext _context;
    GenericDbService<Comment, int> _service;


    [Authorize, HttpPost("Create")]
    public async Task<IActionResult> Create(CreateCommentDTO dto)
    {
        if (!await IsRequestOwnerOrAdmin(dto.VacationRequestId))
        {
            return Forbid();
        }
        var newData = _mapper.Map<Comment>(dto);
        newData.CreationDate = DateTime.Now;
        _context.Add(newData);
        await _context.SaveChangesAsync();

        return Ok();
    }

    [Authorize, HttpGet("ReadAll/{RequestId}")]
    public async Task<ActionResult<IEnumerable<ReadCommentDTO>>> ReadAll(int RequestId)
    {
        if (!await IsRequestOwnerOrAdmin(RequestId))
        {
            return Forbid();
        }
        return Ok(
            from comment in _service.ReadAll() 
            where comment.VacationRequestId == RequestId 
            select _mapper.Map<ReadCommentDTO>(comment));
    }

    [Authorize, HttpGet("ReadById/{id}")]
    public async Task<ActionResult<ReadCommentDTO>> ReadById(int id)
    {
        if (!await IsRequestOwnerOrAdmin(id))
        {
            return Forbid();
        }
        return await _service.ReadById<ReadCommentDTO>(id);
    }
    [Authorize, HttpDelete("Delete/{id}")]
    public async Task<IActionResult> Delete(int id)
    {
        if (!await IsRequestOwnerOrAdmin(id))
        {
            return Forbid();
        }
        if (await _context.Comments.Where(p => p.Id == id).AnyAsync())
        {
            await _service.Delete(id);
            return Ok();
        }
        return BadRequest();
    }

    [Authorize, HttpPut("Update")]
    public async Task<IActionResult> Update(UpdateCommentDTO dto)
    {
        if (!await IsRequestOwnerOrAdmin(dto.Id))
        {
            return Forbid();
        }
        Comment? commentInDb = await _context.Comments.FindAsync(dto.Id);
        if (commentInDb == null)
        {
            return NotFound();
        }

        _context.Update(commentInDb);
        _mapper.Map(dto, commentInDb);
        commentInDb.LastTimeEdited = DateTime.Now;
        await _context.SaveChangesAsync();

        return Ok(commentInDb);
    }
    /// <summary>
    /// Check if the user is the owner of the request which this comment is attached to, OR the user is an admin.
    /// </summary>
    /// <param name="RequestId"></param>
    /// <returns></returns>
    private async Task<bool> IsRequestOwnerOrAdmin(int RequestId)
    {
        var request = await _context.VacationRequests.Where(r => r.Id == RequestId).FirstOrDefaultAsync();

        return (await this.Extension_IsSelfOrAdmin(_context, request?.UserId));
    }
}