﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Tidsbanken_Backend.Models.Data;
using Tidsbanken_Backend.Models.Domain;
using Tidsbanken_Backend.Models.DTOs.Self;
using Tidsbanken_Backend.Services;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace Tidsbanken_Backend.Controllers;
[ApiController]
[Route("User")]
public class SelfUserController : ControllerBase
{

    public SelfUserController(IMapper mapper, TidsbankenDbContext context)
    {
        _mapper = mapper;
        _context = context;
        _service = new GenericDbService<User, string>(context, mapper, (c) => c.Users);
    }
    private GenericDbService<User, string> _service;
    private TidsbankenDbContext _context;
    private IMapper _mapper;

    /// <summary>
    /// Get only the authenticated user, without requiring input.
    /// </summary>
    /// <returns></returns>
    [Authorize, HttpGet("")]
    public async Task<ActionResult<SelfReadUserDTO>> Get() 
    {
        string? userId = this.Extension_GetUserId();
        if (userId == null)
        {
            return BadRequest("User could not be found");
        }
        return await _service.ReadById<SelfReadUserDTO>(userId);
    }
    /// <summary>
    /// If accessed by someone other than self or admin, this should only return name and picture.
    /// </summary>
    [Authorize, HttpGet("ReadById/{id}")]
    public async Task<ActionResult<object>> ReadById(string id)
    {
        var result = await _context.Users.FindAsync(id);
        if (result == null) return NotFound();
        object? output;
        string? userId = this.Extension_GetUserId();
        if (result.Id == userId)
        {
            output = _mapper.Map<SelfReadUserDTO>(result);
        }
        else
        {
            output = _mapper.Map<SelfReadOtherUserDTO>(result);
        }
        return Ok(output);
    }
    [Authorize, HttpDelete("Delete/{id}")]
    public async Task<IActionResult> Delete(string id)
    {
        if (!await this.Extension_IsSelfOrAdmin(_context, id))
        {
            return Forbid();
        }
        if (await _context.Users.Where(u=>u.Id == id).AnyAsync())
        {
            await _service.Delete(id);
            return Ok();
        }
        return BadRequest();
    }
    [Authorize, HttpPut("Update")]
    public async Task<IActionResult> Update(SelfUpdateUserDTO dto)
    {
        if (!await this.Extension_IsSelfOrAdmin(_context, dto.Id))
        {
            return Forbid();
        }
        var oldT = await _context.Users.FindAsync(dto.Id);
        if (oldT == null)
        {
            return NotFound();
        }

        _context.Update(oldT);
        _mapper.Map(dto, oldT);

        await _context.SaveChangesAsync();

        return Ok(oldT);
    }
    [Authorize, HttpGet("GetRequests/{UserId}")]
    public async Task<ActionResult<IEnumerable<VacationRequest>>>? GetRequests(string UserId)
    {
        // TODO Optional: Accept appropriate parameters to limit the search / the number of items returned.
        if (!await this.Extension_IsSelfOrAdmin(_context, UserId))
        {
            return Forbid();
        }
        User? user = await _context.Users
            .Where(u => u.Id == UserId)
            .Include(u => u.VacationRequests)
            .FirstAsync();
        if (user == null)
        {
            return BadRequest("The user could not be found");
        }
        return Ok(user.VacationRequests);
    }
}