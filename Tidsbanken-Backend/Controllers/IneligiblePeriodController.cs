﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Tidsbanken_Backend.Models.Data;
using Tidsbanken_Backend.Models.Domain;
using Tidsbanken_Backend.Models.DTOs.Admin;
using Tidsbanken_Backend.Services;
using Tidsbanken_Backend.Models.DTOs.IneligiblePeriodDTO;
using Microsoft.AspNetCore.Authorization;

namespace Tidsbanken_Backend.Controllers;

[ApiController]
[Route("Ineligible")]
public class IneligiblePeriodController : ControllerBase
{
    public IneligiblePeriodController(IMapper mapper, TidsbankenDbContext context)
    {
        _mapper = mapper;
        _context = context;
        _service = new GenericDbService<IneligiblePeriod, int>(context, mapper, (x) => x.IneligiblePeriods);
    }
    IMapper _mapper;
    TidsbankenDbContext _context;
    GenericDbService<IneligiblePeriod, int> _service;


    [Authorize, HttpPost("Create/")]
    public async Task<IActionResult> Create(CreateIneligiblePeriodDTO dto)
    {
        if (!await this.Extension_IsAdmin(_context))
        {
            return Forbid();
        }
        return await _service.Create(dto);
    }

    [Authorize, HttpGet("ReadAll/")]
    public ActionResult<IEnumerable<ReadIneligiblePeriodDTO>> ReadAll()
    {
        return Ok(from period in _service.ReadAll() select _mapper.Map<ReadIneligiblePeriodDTO>(period));
    }

    [Authorize, HttpGet("ReadById/{id}")]
    public Task<ActionResult<ReadIneligiblePeriodDTO>> ReadById(int id)
    {
        return _service.ReadById<ReadIneligiblePeriodDTO>(id);
    }
    [Authorize, HttpDelete("Delete/{id}")]
    public async Task<IActionResult> Delete(int id)
    {
        if (!await this.Extension_IsAdmin(_context))
        {
            return Forbid();
        }
        if (await _context.IneligiblePeriods.Where(p => p.Id == id).AnyAsync())
        {
            await _service.Delete(id);
            return Ok();
        }
        return BadRequest();
    }

    [Authorize, HttpPut("Update")]
    public async Task<IActionResult> Update(UpdateIneligiblePeriodDTO dto)
    {
        if (!await this.Extension_IsAdmin(_context))
        {
            return Forbid();
        }
        var oldPeriod = await _context.IneligiblePeriods.FindAsync(dto.Id);
        if (oldPeriod == null)
        {
            return NotFound();
        }

        _context.Update(oldPeriod);
        _mapper.Map(dto, oldPeriod);

        await _context.SaveChangesAsync();

        return Ok(oldPeriod);
    }

}
