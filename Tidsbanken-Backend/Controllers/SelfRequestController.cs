﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Tidsbanken_Backend.Models.Data;
using Tidsbanken_Backend.Models.Domain;
using Tidsbanken_Backend.Models.DTOs;
using Tidsbanken_Backend.Models.DTOs.Self;
using Tidsbanken_Backend.Services;

namespace Tidsbanken_Backend.Controllers;

[ApiController]
[Route("Request")]
public class SelfRequestController : ControllerBase
{

    public SelfRequestController(IMapper mapper, TidsbankenDbContext context)
    {
        _mapper = mapper;
        _context = context;
        _service = new GenericDbService<VacationRequest, int>(context, mapper, (x) => x.VacationRequests);
    }
    private IMapper _mapper;
    private TidsbankenDbContext _context;
    private GenericDbService<VacationRequest, int> _service;
    /// <summary>
    /// Creates a VacationRequest and a corresponding VacationStatus.
    /// </summary>
    [Authorize, HttpPost("Create/")]
    public async Task<IActionResult> Create([Bind("Title,StartDate,EndDate,UserId")] SelfCreateVacationRequestDTO dto)
    {
        if (!this.Extension_IsSelf(dto.UserId))
        {
            return Forbid();
        }

        // TODO: Test to make sure that this works correctly.
        Func<IneligiblePeriod, bool> IsOverlapping = (IneligiblePeriod ineligible) =>
                (ineligible.StartDate <= dto.EndDate && ineligible.StartDate >= dto.StartDate) // Ineligible start is wrapped in the period
             || (ineligible.EndDate <= dto.EndDate && ineligible.StartDate >= dto.StartDate) // Ineligible end is wrapped in the period
             || (ineligible.EndDate >= dto.EndDate && ineligible.StartDate <= dto.StartDate) // Ineligible is entirely surrounding the period
             || (ineligible.EndDate <= dto.EndDate && ineligible.StartDate >= dto.StartDate); // Ineligible is entirely surrounded

        var ineligibleOverlap = _context.IneligiblePeriods
            .Where(IsOverlapping);

        if (ineligibleOverlap.Any())
        {
            return BadRequest("This request would overlap with an ineligible period.");
        }


        var newData = _mapper.Map<VacationRequest>(dto);
        var status = new VacationStatus() { Status = StatusHelper.Pending };
        newData.VacationStatus = status;
        _context.Add(newData);
        await _context.SaveChangesAsync();

        return Ok();
    }
    [Authorize, HttpGet("ReadById/{id}")]
    public async Task<ActionResult<SelfReadVacationRequestDTO>> ReadById(int id)
    {
        // User must be admin, OR the request must be approved, OR user must own the request.
        var request = await _context.VacationRequests.FindAsync(id);
        var DTO = _mapper.Map<SelfReadVacationRequestDTO>(request);

        if (await this.Extension_IsSelfOrAdmin(_context, request.UserId))
        {
            return Ok(DTO);
        }

        var status = await GetVacationStatus(id);
        if (status == null || status.Value == null)
        {
            return BadRequest("Couldn't find the request status");
        }
        if (status.Value.Status == StatusHelper.Approved)
        {
            return Ok(DTO);
        }
        return Forbid();
    }
    [Authorize, HttpPut("Update")]
    public async Task<IActionResult> Update(SelfUpdateVacationRequestDTO dto)
    {
        var oldT = await _context.VacationRequests.Include(v => v.VacationStatus).Where(v => v.Id == dto.Id).FirstAsync();
        if (!this.Extension_IsSelf(oldT?.UserId))
        {
            return Forbid("Can't edit requests that belong to other users.");
        }
        if (oldT == null)
        {
            return NotFound();
        }
        if (oldT.VacationStatus.Status != StatusHelper.Pending)
        {
            return Forbid("Can't edit a request that is not pending.");
        }

        _context.Update(oldT);
        _mapper.Map(dto, oldT);


        await _context.SaveChangesAsync();

        return Ok(oldT);
    }
    [Authorize, HttpGet("ReadAll/")]
    public ActionResult<IEnumerable<SelfReadVacationRequestDTO>> ReadAll()
    {
        // TODO optionally: Limit this to pages of 50 at a time.

        string? userId = this.Extension_GetUserId();
        if (userId == null)
        {
            return BadRequest("User could not be found");
        }


        return Ok(
            from request in _service.ReadAll()
            where request.UserId == userId
            select _mapper.Map<SelfReadVacationRequestDTO>(request));
    }
    [Authorize, HttpGet("ReadAllApproved/")]
    public ActionResult<IEnumerable<SelfReadVacationRequestDTO>> ReadAllApproved()
    {
        // View all requests which have been approved. Can users see other users' accepted requests?
        // TODO optionally: Limit this to pages of 50 at a time.

        return Ok(_service.ReadAll()
            .Include(r => r.VacationStatus)
            .Where(r => r.VacationStatus.Status == StatusHelper.Approved)
            .Select(r => _mapper.Map<SelfReadVacationRequestDTO>(r)));
    }
    [Authorize, HttpGet("GetStatus/{RequestId}")]
    public async Task<ActionResult<VacationStatus>> GetVacationStatus(int RequestId)
    {
        string? userId = this.Extension_GetUserId();
        if (userId == null)
        {
            return BadRequest("User could not be found");
        }
        return (await _context.VacationRequests
            .Include(i => i.VacationStatus)
            .Where(i => i.Id == RequestId && i.UserId == userId)
            .FirstAsync()).VacationStatus;
    }
}
