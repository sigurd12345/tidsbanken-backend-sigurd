﻿
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using System.Diagnostics;
using System.Net.Http.Headers;
using Tidsbanken_Backend.Models.Data;
using Tidsbanken_Backend.Models.Domain;
using Tidsbanken_Backend.Services;

namespace Tidsbanken_Backend.Controllers
{
    [ApiController]
    [Route("Test")]
    public class APITests : ControllerBase
    {
        public APITests(IMapper mapper, TidsbankenDbContext context)
        {
            _mapper = mapper;
            _context = context;
        }
        private IMapper _mapper;
        private TidsbankenDbContext _context;
        [Authorize, HttpGet("Authorize")]
        public IActionResult Get()
        {
            var token = ManagementAccess.GetManagementAccessToken();
            return Ok(token.Content);
        }

        [HttpPut("AddMeToTheDatabase")]
        public async Task<IActionResult> AddMeToTheDatabase()
        {
            if (!Debugger.IsAttached)
            {
                return BadRequest("This endpoint is only available during debug.");
            }
            var Id = this.Extension_GetUserId();

            if (!_context.Users.Where(x=>x.Id == Id).Any())
            {
                return BadRequest("User already exists");
            }


            var newData = new User();
            newData.Id = Id;
            newData.FirstName = "TEST";
            newData.LastName = "TEsT";
            newData.IsAdmin = true;
            newData.ProfilePicture = "url";
            newData.Email = "@";
            newData.PhoneNumber = 0;

            _context.Add(newData);
            await _context.SaveChangesAsync();

            return Ok();
        }

        // Test om vi klarer å hente token fra requestet.
        // https://stackoverflow.com/questions/63209962/fetch-access-token-from-authorization-header-without-bearer-prefix
        [Authorize, HttpGet("GetToken")]
        public ActionResult<string?> GetToken()
        {
            var authorization = Request.Headers[HeaderNames.Authorization];
            if (AuthenticationHeaderValue.TryParse(authorization, out var headerValue))
            {
                return headerValue.Parameter; // Dette er token
            }
            return NotFound();
        }

        [Authorize, HttpGet("GetBearer")]
        public ActionResult<string?> GetScheme()
        {
            var authorization = Request.Headers[HeaderNames.Authorization];
            if (AuthenticationHeaderValue.TryParse(authorization, out var headerValue))
            {
                return headerValue.Scheme; // Dette er bearer
            }
            return NotFound();
        }

        [Authorize, HttpGet("GetUserId")]
        public ActionResult<string?> GetUserId()
        {
            return this.Extension_GetUserId();
        }
        [Authorize, HttpGet("GetIsAdmin")]
        public async Task<bool> GetIsAdmin()
        {
            return await this.Extension_IsAdmin(_context);
        }
    }
}